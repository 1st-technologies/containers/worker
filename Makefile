.PHONY: publish build push clean run stop test lint
.DEFAULT_GOAL:=help

REGISTRY:=1sttec
IMAGE_NAME:=worker
IMAGE_VERSION:=0.1.1

## make help that works in either Linux or Windows? you're welcome
help: ## Show this help
ifeq ($(OS),Windows_NT)
	@echo Usage:
	@echo   make ^<target^>
	@cmd /c echo.
	@echo Targets:
	@for /f "tokens=1,3* delims=:#" %%a in ('@find " ##" ^< $(MAKEFILE_LIST) ^| find /v "find"') do echo   [93m%%a[0m:%%b
	@cmd /c echo.
else
	@echo "Usage:"
	@echo "  make <target>"
	@echo ""
	@echo "Targets:"
	@sed -ne '/@sed/!s/## //p' $(MAKEFILE_LIST) | sed -e 's/^/  /'
	@echo ""
endif

publish: lint build push clean ## Run the build, push and clean targets

build: ## Build image
	@docker build --no-cache -t ${REGISTRY}/${IMAGE_NAME}:latest -t ${REGISTRY}/${IMAGE_NAME}:${IMAGE_VERSION} .

push: ## Push image to internal repo
	@docker login --username ${USERID} --password ${PASSWORD}
	@docker push ${REGISTRY}/${IMAGE_NAME}:${IMAGE_VERSION}
	@docker push ${REGISTRY}/${IMAGE_NAME}:latest
	@docker logout

clean: ## Delete image
	@docker rmi ${REGISTRY}/${IMAGE_NAME}:${IMAGE_VERSION}
	@docker rmi ${REGISTRY}/${IMAGE_NAME}:latest

run: ## Run service
	@docker run --detach --rm --name ${IMAGE_NAME} -p 8000:8000 ${REGISTRY}/${IMAGE_NAME} -u ${TEAMS_URL} -l ${LISTENER} -p ${PORT} --key "${KEY}"
ifeq ($(OS),Windows_NT)
	@timeout 3 > NUL
else
	@sleep 3
endif

lint: ## check Dockerfile
	@hadolint --failure-threshold error ./Dockerfile

test: ## Open a test instance of the container
	@docker run -ti --rm ${REGISTRY}/${IMAGE_NAME}:latest /bin/bash

stop: ## Stop the container
ifeq ($(OS),Windows_NT)
	@docker stop ${IMAGE_NAME} > NUL
else
	@docker stop ${IMAGE_NAME} > /dev/null
endif
