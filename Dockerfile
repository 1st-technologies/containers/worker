FROM rockylinux:9.0.20220720

RUN yum update -y
RUN yum install python3-pip-21.2.3 krb5-workstation-1.19.1 git-2.31.1 -y && yum clean all
RUN python3 -m pip install --no-cache-dir --upgrade pip==22.3.1 \
  && python3 -m pip install --no-cache-dir ansible==7.0.0 ansible-lint==6.9.1